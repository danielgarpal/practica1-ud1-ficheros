package com.danielgarpal.tiendaCafeYTe.Util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class Util {

    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje,"Oh no. Oh no. Oh no no no no no", JOptionPane.ERROR_MESSAGE);
    }

    public static int mensajeConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null, mensaje, titulo,JOptionPane.YES_NO_OPTION);
    }

    public static void mensajeAbout() {
        JOptionPane.showMessageDialog(null, "CREADO POR:\n\tDaniel García\nCURSO\n\t2DAM 2021-2022",
                "Información", JOptionPane.INFORMATION_MESSAGE);
    }
    public static JFileChooser crearSelectorFichero(File ruta, String tipoArchivo, String extension) {
        JFileChooser selectorFichero=new JFileChooser();
        if(ruta!=null) {
            selectorFichero.setCurrentDirectory(ruta);
        }
        if (extension!=null) {
            FileNameExtensionFilter filtro = new
                    FileNameExtensionFilter(tipoArchivo,extension);
            selectorFichero.setFileFilter(filtro);
        }

        return selectorFichero;

    }


}
