package com.danielgarpal.tiendaCafeYTe;

import com.danielgarpal.tiendaCafeYTe.gui.ProductosControlador;
import com.danielgarpal.tiendaCafeYTe.gui.ProductosModelo;
import com.danielgarpal.tiendaCafeYTe.gui.Ventana;

public class Principal {
    public static void main(String[] args) {
        Ventana vista = new Ventana();
        ProductosModelo modelo = new ProductosModelo();
        ProductosControlador controlador = new ProductosControlador(vista,modelo);

    }
}
