package com.danielgarpal.tiendaCafeYTe.base;

public abstract class Producto {

private String ID;
private String marca;
private String origen;
private int intensidad;

//fecha molido o recolección en los hijos

    public Producto(){
        this.ID = "";
        this.marca = "";
        this.origen = "";
        this.intensidad = 1;
    }

    public Producto(String ID, String marca, String origen, int intensidad) {
        this.ID = ID;
        this.marca = marca;
        this.origen = origen;
        this.intensidad = intensidad;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public int getIntensidad() {
        return intensidad;
    }

    public void setIntensidad(int intensidad) {
        this.intensidad = intensidad;
    }
}
