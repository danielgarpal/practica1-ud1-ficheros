package com.danielgarpal.tiendaCafeYTe.base;

import java.time.LocalDate;

public class Cafe extends Producto{
     private LocalDate fechaMolido;

    public Cafe() {
        super();
        fechaMolido=null;
    }


    public Cafe(String id, String marca, String origen, LocalDate fechaMolido, int intensidad) {
        super(id, marca, origen, intensidad);
        this.fechaMolido = fechaMolido;
    }

    public LocalDate getFechaMolido() {
        return fechaMolido;
    }

    public void setFechaMolido(LocalDate fechaMolido) {
        this.fechaMolido = fechaMolido;
    }


    @Override
    public String toString() {
        return "Cafe{" +
                "id: " + getID() +
                ",fechaMolido=" + fechaMolido +
                ",marca:" + getMarca() +
                ",origen: " + getOrigen()+
                ",intensidad: " + getIntensidad() +
                '}';
    }
}
