package com.danielgarpal.tiendaCafeYTe.base;

import java.time.LocalDate;

public class Te extends Producto{
private LocalDate fechaRecogida;



    public Te() {
        super();
        fechaRecogida=null;
    }



    public Te(String id, String marca, String origen, LocalDate fechaRecoleccion, int intensidad) {
        super(id, marca, origen, intensidad);
        this.fechaRecogida = fechaRecoleccion;
    }

    public LocalDate getFechaRecogida() {
        return fechaRecogida;
    }

    public void setFechaRecogida(LocalDate fechaRecogida) {
        this.fechaRecogida = fechaRecogida;
    }

    @Override
    public String toString() {
        return "Te{" +
                "id: " + getID() +
                ",marca:" + getMarca() +
                ",origen: " + getOrigen() +
                ",intensidad: " + getIntensidad() +
                ",fechaRecogida=" + fechaRecogida +
                '}';
    }
}
