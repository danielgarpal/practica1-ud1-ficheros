package com.danielgarpal.tiendaCafeYTe.gui;

import com.danielgarpal.tiendaCafeYTe.base.Cafe;
import com.danielgarpal.tiendaCafeYTe.base.Producto;
import com.danielgarpal.tiendaCafeYTe.base.Te;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ProductosModelo {

    private ArrayList<Producto> listaProductos;

    public ProductosModelo() {
       listaProductos = new ArrayList<Producto>();
    }

    public ArrayList<Producto> obtenerListaProductos(){
        return listaProductos;

    }


    public void altaCafe(String id, String marca, String origen, LocalDate fechaMolido, int intensidad) {
        Cafe nuevoCafe = new Cafe(id,marca,origen,fechaMolido,intensidad);
        listaProductos.add(nuevoCafe);

    }

    public void altaTe(String id, String marca, String origen, LocalDate fechaRecoleccion, int intensidad) {
        Te nuevoTe = new Te(id,marca,origen,fechaRecoleccion,intensidad);
        listaProductos.add(nuevoTe);
    }
    public boolean existeProducto(String id){
        for (Producto unProducto : listaProductos){
            if(unProducto.getID().equalsIgnoreCase(id)){
                return true;
            }
        }
        return false;
    }

    public void exportar(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document doc =dom.createDocument(null, "xml",null);


        Element raiz = doc.createElement("Productos");
        doc.getDocumentElement().appendChild(raiz);

        Element nodoProducto = null;
        Element nodoDatos = null;
        Text texto = null;

        for (Producto unProducto : listaProductos){

            if (unProducto instanceof Cafe){
                nodoProducto=doc.createElement("Cafe");
            }else{
                nodoProducto=doc.createElement("Te");
            }
            raiz.appendChild(nodoProducto);

            //ID
            nodoDatos=doc.createElement("id");
            nodoProducto.appendChild(nodoDatos);
            texto=doc.createTextNode(unProducto.getID());
            nodoDatos.appendChild(texto);

            //MARCA
            nodoDatos=doc.createElement("marca");
            nodoProducto.appendChild(nodoDatos);
            texto=doc.createTextNode(unProducto.getMarca());
            nodoDatos.appendChild(texto);

            //ORIGEN
            nodoDatos=doc.createElement("origen");
            nodoProducto.appendChild(nodoDatos);
            texto=doc.createTextNode(unProducto.getOrigen());
            nodoDatos.appendChild(texto);

            //FECHA
            if (unProducto instanceof Cafe){
                nodoDatos=doc.createElement("fecha-molido");
                nodoProducto.appendChild(nodoDatos);
                texto=doc.createTextNode(((Cafe) unProducto).getFechaMolido().toString());
                nodoDatos.appendChild(texto);
            }else{
                nodoDatos=doc.createElement("fecha-recogida");
                nodoProducto.appendChild(nodoDatos);
                texto=doc.createTextNode(((Te) unProducto).getFechaRecogida().toString());
                nodoDatos.appendChild(texto);
            }
            //INTENSIDAD
            nodoDatos=doc.createElement("intensidad");
            nodoProducto.appendChild(nodoDatos);
            texto=doc.createTextNode(String.valueOf(unProducto.getIntensidad()));
            nodoDatos.appendChild(texto);

            Source source = new DOMSource(doc);
            Result resultado=new StreamResult(fichero);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source,resultado);
        }





    }

    public void importar(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaProductos=new ArrayList<Producto>();
        Cafe nuevoCafe=null;
        Te nuevoTe=null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder=factory.newDocumentBuilder();
        Document documento=builder.parse(fichero);



        NodeList listaNodos = documento.getElementsByTagName("*");
        for (int i=0;i<listaNodos.getLength();i++) {
            Element nodoProducto = (Element) listaNodos.item(i);

            if (nodoProducto.getTagName().equals("Cafe")) {
                nuevoCafe = new Cafe();
                nuevoCafe.setID(nodoProducto.getChildNodes().item(0).getTextContent());
                nuevoCafe.setMarca(nodoProducto.getChildNodes().item(1).getTextContent());
                nuevoCafe.setOrigen(nodoProducto.getChildNodes().item(2).getTextContent());
                nuevoCafe.setFechaMolido(LocalDate.parse(nodoProducto.getChildNodes().item(3).getTextContent()));
                nuevoCafe.setIntensidad(Integer.parseInt(nodoProducto.getChildNodes().item(4).getTextContent()));
              listaProductos.add(nuevoCafe);
            }else if (nodoProducto.getTagName().equals("Te")) {
                    nuevoTe=new Te();
                    nuevoTe.setID(nodoProducto.getChildNodes().item(0).getTextContent());
                    nuevoTe.setMarca(nodoProducto.getChildNodes().item(1).getTextContent());
                    nuevoTe.setOrigen(nodoProducto.getChildNodes().item(2).getTextContent());
                    nuevoTe.setFechaRecogida(LocalDate.parse(nodoProducto.getChildNodes().item(3).getTextContent()));
                    nuevoTe.setIntensidad(Integer.parseInt(nodoProducto.getChildNodes().item(4).getTextContent()));
                    listaProductos.add(nuevoCafe);

            }

        }
    }
}
