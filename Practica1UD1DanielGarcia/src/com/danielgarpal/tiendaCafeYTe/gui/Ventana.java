package com.danielgarpal.tiendaCafeYTe.gui;

import com.danielgarpal.tiendaCafeYTe.base.Producto;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Ventana {
    public JPanel panel;
    public JRadioButton rbte;
    public JTextField tfId;
    public JTextField tfMarca;
    public JTextField tfOrigen;
    public JSlider sIntensidad;
    public JButton bGuardar;
    public JButton bImportar;
    public JList lLista;
    public JButton bAbout;
    public JButton bExportar;
    public JRadioButton rbcafe;
    public DatePicker dpFecha;
    public JLabel fnFecha;


    public JFrame frame;
    public DefaultListModel<Producto> dlmProducto;


    public Ventana() {
        frame = new JFrame("CafeyTeMVC");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        iniciarComponentes();

    }
    private void iniciarComponentes() {
        dlmProducto=new DefaultListModel<Producto>();
        lLista.setModel(dlmProducto);

    }
}
