package com.danielgarpal.tiendaCafeYTe.gui;

import com.danielgarpal.tiendaCafeYTe.Util.Util;
import com.danielgarpal.tiendaCafeYTe.base.Cafe;
import com.danielgarpal.tiendaCafeYTe.base.Producto;
import com.danielgarpal.tiendaCafeYTe.base.Te;
import javafx.scene.control.Slider;
import org.xml.sax.SAXException;

import javax.print.attribute.standard.JobPriority;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ProductosControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private ProductosModelo modelo;

    private File ruta;


    public ProductosControlador(Ventana vista, ProductosModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            System.out.println("No existe el fichero " + e.getMessage());
        }


        addActionListener(this);
        addListSelectionListener(this);
        addWindowsListener(this);
    }

    //-----------------LISTENERS--------------------
    private void addWindowsListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.lLista.addListSelectionListener(listener);
    }

    private void addActionListener(ActionListener listener) {
        vista.rbcafe.addActionListener(listener);
        vista.rbte.addActionListener(listener);
        vista.bAbout.addActionListener(listener);
        vista.bExportar.addActionListener(listener);
        vista.bImportar.addActionListener(listener);
        vista.bGuardar.addActionListener(listener);

    }
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("rutas.conf"));
        ruta = new File(configuracion.getProperty("ruta"));
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch(actionCommand){
            case "Guardar":
                if (camposVacios()==true) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios \n" +
                            "ID \n Marca \n Origen \n Fecha " );
                    break;
                }

                if (modelo.existeProducto(vista.tfId.getText())) {
                    Util.mensajeError("Ya hay un producto con este id\n\n En: " + vista.tfId.getText());
                    break;
                }
                if (vista.rbcafe.isSelected()) {
                    modelo.altaCafe(vista.tfId.getText(),
                            vista.tfMarca.getText(),
                            vista.tfOrigen.getText(),
                            vista.dpFecha.getDate(),
                            vista.sIntensidad.getValue());
                } else if (vista.rbte.isSelected()){
                    modelo.altaTe(vista.tfId.getText(),
                            vista.tfMarca.getText(),
                            vista.tfOrigen.getText(),
                            vista.dpFecha.getDate(),
                            vista.sIntensidad.getValue());
                }else{
                    Util.mensajeError("No has seleccionado un tipo de producto");
                }
                limpiarCampos();
                refrescar();
                //System.out.println(modelo.obtenerListaProductos());
                break;
            case "Exportar":
                JFileChooser selectorFichero = Util.crearSelectorFichero(ruta,"Archivos XML","xml");
                int opcion1=selectorFichero.showSaveDialog(null);
                if (opcion1==JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportar(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException | TransformerException e1) {
                        e1.printStackTrace();
                    }
                }
                break;
            case "Importar": JFileChooser selectorFichero2 = Util.crearSelectorFichero(ruta, "Archivos XML","xml");
                int opcion2=selectorFichero2.showOpenDialog(null);
                if (opcion2==JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importar(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Cafe":vista.fnFecha.setText("Fecha molido");
                break;
            case "Te":vista.fnFecha.setText("Fecha recolección");
                break;
            case "About":
                Util.mensajeAbout();
                break;
        }


    }

    private boolean camposVacios() {
        if (vista.tfId.getText().isEmpty() ||
                vista.tfMarca.getText().isEmpty() ||
                vista.tfOrigen.getText().isEmpty() ||
                vista.dpFecha.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    private void refrescar() {
        vista.dlmProducto.clear();
        for (Producto unProducto :  modelo.obtenerListaProductos()){
            vista.dlmProducto.addElement(unProducto);
        }




    }

    private void limpiarCampos() {
        vista.tfId.setText("");
        vista.tfMarca.setText("");
        vista.tfOrigen.setText("");
        vista.dpFecha.setText("");
        vista.sIntensidad.setValue(1);
    }



    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        int respuesta ;
        respuesta= Util.mensajeConfirmacion("¿Quieres cerrar esta ventana?", "Salir");
        if(respuesta== JOptionPane.YES_OPTION){
            System.exit(0);


        }

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Producto esteProducto = (Producto) vista.lLista.getSelectedValue();
            vista.tfId.setText(esteProducto.getID());
            vista.tfOrigen.setText(esteProducto.getOrigen());
            vista.tfMarca.setText(esteProducto.getMarca());
            vista.sIntensidad.setValue(esteProducto.getIntensidad());
            if (esteProducto instanceof Cafe) {
                vista.dpFecha.setDate(((Cafe) esteProducto).getFechaMolido());
                vista.rbcafe.doClick();
            } else {
                vista.rbte.doClick();
                vista.dpFecha.setDate(((Te) esteProducto).getFechaRecogida());
            }
        }
    }
}
